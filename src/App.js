import avatar from "./assets/images/avatar.jpg";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css"; //import bootstrap
function App() {
  return (
    <div>
      <div className="devcamp-container container">
        <div>
          <img className="devcamp-avatar" src={avatar} alt="Avatar"/>
        </div>
        <div>
          <p className="devcamp-quote">This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
        </div>
        <div className="devcamp-author">
          <b className="devcamp-name">
            Tammy Stevens 
          </b>
          &nbsp; * &nbsp;Front End Developer
        </div>
      </div>
    </div>

  );
}

export default App;
